//
//  main.cpp
//  prob1
//
//  Created by John Defourneaux on 1/9/13.
//  Copyright (c) 2013 John Defourneaux. All rights reserved.
//

#include <iostream>

using namespace std;

const int START = 1000;

int main()
{

    int num;
    int sum = 0;
    
    for (num = 0; num < START; num++) {
        // Check if the num is a multiple of 3 or 5
        if ((num % 3 == 0) || (num % 5 == 0)) {
            sum = sum + num;
        }
    }
    
    cout << sum << endl;
    
    return 0;
}

