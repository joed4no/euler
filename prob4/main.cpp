//
//  main.cpp
//  prob4
//
//  Created by John Defourneaux on 1/11/13.
//  Copyright (c) 2013 John Defourneaux. All rights reserved.
//
//  A palindromic number reads the same both ways. The largest palindrome
//  made from the product of two 2-digit numbers is 9009 = 91 * 99.
//
//  Find the largest palindrome made from the product of two 3-digit numbers.

#include <iostream>
#include <string>
#include <sstream>

using namespace std;


int main()
{

    int firstNum; // the first 3-digit number
    int secondNum; // the second 3-digit number
    int prod; // the product of the two numbers
    string strProd; // the string version of the product
    int len; // length of prod

    stringstream convert;
    
    int largest = 0; // the largest palindrome number so far
    
    for (firstNum = 999; firstNum > 100; firstNum--)
    {
        for (secondNum = 999; secondNum > 100; secondNum--)
        {
            prod = 0;
            prod = firstNum * secondNum;
            
            convert.str("");
            convert << prod;
            strProd = convert.str();
            len = strProd.size();
            
            int start = 0; // first position of the number string
            int end = len - 1; // last position of the number string
            bool isPalindrome = false; // true/false flag
            int aPalin;
            
            while (start < len)
            {
                if (strProd.at(start) == strProd.at(end))
                {
                    isPalindrome = true;
                }
                else
                {
                    isPalindrome = false;
                    break;
                }
                start++;
                end--;
            }
            
            if (isPalindrome == true)
            {
                aPalin = atoi(strProd.c_str());
                if (aPalin > largest)
                {
                    largest = aPalin;
                }
            }
        }
    }
    
    cout << "Answer:" << largest << endl;
    
    return 0;
}
