//
//  main.cpp
//  prob3
//
//  Created by John Defourneaux on 1/9/13.
//  Copyright (c) 2013 John Defourneaux. All rights reserved.
//
//  The prime factors of 13195 are 5, 7, 13 and 29.
//
//  What is the largest prime factor of the number 600851475143 ?

#include <iostream>
#include <cmath>

using namespace std;

bool PrimeFinder(long);


int main()
{

    const long MAX = 600851475143;
    long num;
    bool primeFlag;
    
    for (num = 3; num < MAX; num++)
    {
        primeFlag = PrimeFinder(num);
        
        if (primeFlag)
        {
            //cout << num << endl;
            if (MAX % num == 0)
            {
                cout << num << " is a prime factor." << endl;
            }
        }
    }
    

    return 0;
}


bool PrimeFinder(long num)
// Determines if a number is prime
{
    int counter;
    bool isPrime = true;
    
    for (counter = 2; counter < num; counter++)
    {
        if (num % counter == 0)
        {
            isPrime = false;
            
            return isPrime;
        }
    }
    
    return isPrime;
}