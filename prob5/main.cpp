//
//  main.cpp
//  prob5
//
//  Created by John Defourneaux on 1/9/13.
//  Copyright (c) 2013 John Defourneaux. All rights reserved.
//

#include <iostream>

using namespace std;

int main()
{

    int num = 1; // number to increment looking for answer
    bool answer = false; // while loop flag
    int count;
    
    while (!answer)
    {
        count = 1;
        while ((num % count == 0) && count <= 20)
        {
            if (count == 20)
            {
                answer = true;
                cout << "Answer: " << num << endl;
            }
            count++;
        }
        
        num++;
    }
    
    return 0;
}

