//
//  main.cpp
//  prod10
//
//  Created by John Defourneaux on 1/22/13.
//  Copyright (c) 2013 John Defourneaux. All rights reserved.
//
//  The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
//
//  Find the sum of all the primes below two million.

#include <iostream>
#include <math.h>

using namespace std;

bool IsPrime(int);


int main()
{
    int num;
    long long sum = 2;
    
    for (num = 3; num < 2000000; num += 2)
    {        
        if (IsPrime(num))
        {
            sum += num;
        }
    }
    
    cout << "Answer:" << sum << endl;
    
    return 0;
}


bool IsPrime(int num)
// Determines if a number is prime
{
    if (num <=1)
        return false;
    else if (num == 2)
        return true;
    else if (num % 2 == 0)
        return false;
    else
    {
        int divisor = 3;
        double num_d = static_cast<double>(num);
        int upperLimit = static_cast<int>(sqrt(num_d) +1);
        
        while (divisor <= upperLimit)
        {
            if (num % divisor == 0)
            {
                return false;
            }
            divisor +=2;
        }
        return true;
    }
}