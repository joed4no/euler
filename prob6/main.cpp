//
//  main.cpp
//  prob3
//
//  Created by John Defourneaux on 1/9/13.
//  Copyright (c) 2013 John Defourneaux. All rights reserved.
//

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    
    int sumOfSqr = 0; // sum of the squares
    int sum = 0; // sum of the numbers up to 100
    int sqrOfSum = 0; // square of the sum
    int diff; // difference between the sum of the squares and square of the sum
    int count; // loop counter
    
    for (count = 1; count <= 100; count++)
    {
        sumOfSqr = sumOfSqr + pow(count, 2);
        
        sum = sum + count;
    }
    
    sqrOfSum = pow(sum, 2);
    
    diff = sqrOfSum - sumOfSqr;
    
    cout << sqrOfSum << " " << sumOfSqr << endl;
    
    cout << diff << endl;
    
    
    return 0;
}

