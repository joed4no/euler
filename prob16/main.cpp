//
//  main.cpp
//  prob16
//
//  Created by John Defourneaux on 1/23/13.
//  Copyright (c) 2013 John Defourneaux. All rights reserved.
//
//  215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
//  What is the sum of the digits of the number 21000?

#include "iostream"
#include "cmath"
#include "iomanip"
#include "string"
#include "sstream"

using namespace std;

int main()
{

    long double longNum;
    ostringstream converter;
    string strNum;
    
    longNum = pow(2, 1000);

    converter << fixed << setprecision(0) << longNum;
    strNum = converter.str();
    
    int digit;
    int total = 0;
    for (int count = 0; count < strNum.length(); count++)
    {
        istringstream(strNum.substr(count,1)) >> digit;
        total += digit;
        
    }
    
    cout << "Resulting sum: " << total << endl;
    
    return 0;
}

