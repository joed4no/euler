//
//  main.cpp
//  prob2
//
//  Created by John Defourneaux on 1/9/13.
//  Copyright (c) 2013 John Defourneaux. All rights reserved.
//

#include <iostream>

using namespace std;

const long MAX = 4000000;

int main()
{

    long prev; // holds the previous term in the sequence
    long current; // holds the current term in the sequence
    long next; // holds the next term in the sequence
    long sum; // holds the sum of the even numbered terms
    
    prev = 1;
    current = 1;
    
    while (current < MAX)
    {
        next = current + prev;
        prev = current;
        current = next;
        
        if ((current % 2) == 0)
        {
            sum = sum + current;
        }
    }
    
    cout << sum << endl;
    
    return 0;
}

