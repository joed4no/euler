//
//  main.cpp
//  prob7
//
//  Created by John Defourneaux on 1/11/13.
//  Copyright (c) 2013 John Defourneaux. All rights reserved.
//
//  By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
//  we can see that the 6th prime is 13.
//
//  What is the 10 001st prime number?

#include <iostream>

using namespace std;

bool IsPrime(int);

int main()
{
    int number = 14;
    int count = 6;
    bool isItPrime;
    
    while (count < 10001)
    {
        isItPrime = false;
        isItPrime = IsPrime(number);
        
        if (isItPrime == true)
        {
            count++;
        }
        
        number++;
    }
    cout << "Answer:" << number - 1 << endl;
    
    return 0;
}


bool IsPrime(int num)
{
    int check = 2;
    bool isPrime = true;
    
    while (check < num)
    {
        if (num % check == 0)
        {
            isPrime = false;
            break;
        }
        check++;
    }
    
    return isPrime;
}

