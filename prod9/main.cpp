//
//  main.cpp
//  prod9
//
//  Created by John Defourneaux on 1/14/13.
//  Copyright (c) 2013 John Defourneaux. All rights reserved.
//
//  A Pythagorean triplet is a set of three natural numbers, a  b  c, for which
//  a^2 + b^2 = c^2
//  For example, 3^2 + 4^2 = 9 + 16 = 25 = 52.
//
//  There exists exactly one Pythagorean triplet for which a + b + c = 1000.
//  Find the product abc.

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int a;
    int b;
    int c;
    const int TOTAL = 1000;
    
    for (c = 1; c <= 500; c++)
    {
        for (a = 1; a <= 500; a++)
        {
            for (b = 1; b <= 500; b++)
            {
                if (pow(a, 2) + pow(b, 2) == pow(c, 2))
                {
                    if ((a + b + c) == TOTAL)
                    {
                        cout << "Answer:" << (a * b * c) << endl;
                        return 0;
                    }
                }
            }
        }
    }
    
    
    return 0;
}